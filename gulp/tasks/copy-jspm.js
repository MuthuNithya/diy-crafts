/**
 * Created by nithyarad on 7/21/16.
 */

'use strict';

import gulp from 'gulp';
import path from '../paths';

gulp.task('jspm-resources', function(){
   return gulp.src(path.app.jspm)
       .pipe(gulp.dest(path.build.dist.jspm));
});

gulp.task('jspm-systemjs', ['jspm-config'], function(){
    return gulp.src(path.app.jspmSystem)
        .pipe(gulp.dest(path.build.dist.jspm));
});

gulp.task('jspm-config', function(){
   return gulp.src(path.app.jspmSystemConfig)
       .pipe(gulp.dest(path.build.dist.basePath));
});