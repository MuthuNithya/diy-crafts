/**
 * Created by nithyarad on 7/21/16.
 */

'use strict';

import gulp from 'gulp';
import path from '../paths';

gulp.task('copy-data', function(){
    return gulp.src(path.app.data)
        .pipe(gulp.dest(path.build.dist.data));
});