/**
 * Created by nithyarad on 7/19/16.
 */

'use strict';

var path = require('path');
var ScreenShotReporter = require('protractor-html-screenshot-reporter');

exports.config = {
    seleniumArgs: ['-browserTimeout = 60'],
    capabilities: {
        'phantomjs.binary.path': './node_modules/phantomjs/bin/phantomjs',
        'phantomjs.cli.args': ['--ignore-ssl-errors=true', '--web-security=false'],
        'version': '',
        'platform': 'ANY'
    },

    /**
     * A callback function called once protractor is ready and avaialble,
     * and before the specs are executed.
     * 
     * You can specify a file containing code to run by setting onPrepare to the filename string.
     */
    onPrepare: function(){
        browser.driver.manage().window().setSize(1024, 768);

        /**
         * At this point, global 'protractor' object will be set up, and 
         * jasmine will be available.
         * 
         * The require statement must be down here, since jasmine-reporters
         * needs jasmine to be in the global and protractor does not guarantee
         * this until inside the onPrepare function.
         */
        var jasmineReporters = required('jasmine-reporters');
        jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
            consolidateAll: true,
            filePrefix: 'xmloutput',
            savePath: './test-reports/e2e-test-report'
        }));
        
        jasmine.getEnv().addReporter(new ScreenShotReporter({
            baseDirectory: './test-reports/screenshots',
            pathBuilder: function pathBuilder(spec, descriptions, results, capabilities){
                return path.join(capabilities.caps_.browserName, descriptions.join('-'));
            },
            takeScreenShotsOnlyForFailedSpecs: true
        }));
    },
    
    jasmineNodeOpts:{
        //If true display spec names
        isVerbose: true
    }
};