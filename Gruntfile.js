module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-html-snapshot');

    grunt.initConfig({
        htmlSnapshot: {
            all: {
                options: {
                    snapshotPath: 'snapshots/',
                    sitePath: 'http://diycraftsnme.com/index-build.html',
                    urls: ['/']
                }
            }
        }
    });

    grunt.registerTask('default', ['htmlSnapshot']);
};