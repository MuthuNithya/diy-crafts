SystemJS.config({
 baseURL: ".",
 production: true,
 paths: {
  "npm:": "jspm_packages/npm/",
  "github:": "jspm_packages/github/",
  "bower:": "jspm_packages/bower/",
  "diy-crafts/": "src/"
 },
 bundles: {
  "app.build.js": [
   "diy-crafts/app/common/app.js",
   "diy-crafts/app/common/app.config.js",
   "diy-crafts/app/contact-section/recommend.html!npm:plugin-ng-template@0.1.1/ng-template.js",
   "github:angular/bower-angular@1.5.8/angular.js",
   "github:angular/bower-angular@1.5.8.json",
   "npm:plugin-ng-template@0.1.1.json",
   "diy-crafts/app/contact-section/contact-us.html!npm:plugin-ng-template@0.1.1/ng-template.js",
   "diy-crafts/app/projects/project-details/project-details.html!npm:plugin-ng-template@0.1.1/ng-template.js",
   "diy-crafts/app/search/search.html!npm:plugin-ng-template@0.1.1/ng-template.js",
   "diy-crafts/app/home/home-projects.html!npm:plugin-ng-template@0.1.1/ng-template.js",
   "diy-crafts/app/home/home.html!npm:plugin-ng-template@0.1.1/ng-template.js",
   "diy-crafts/app/layout/header.html!npm:plugin-ng-template@0.1.1/ng-template.js",
   "diy-crafts/app/projects/project-details/details-controller.js",
   "npm:systemjs-plugin-babel@0.0.12/babel-helpers/createClass.js",
   "npm:systemjs-plugin-babel@0.0.12.json",
   "npm:systemjs-plugin-babel@0.0.12/babel-helpers/classCallCheck.js",
   "diy-crafts/app/projects/project-controller.js",
   "diy-crafts/app/common/consts.js",
   "diy-crafts/app/projects/project-service.js",
   "diy-crafts/app/contact-section/contact-controller.js",
   "diy-crafts/app/home/home-service.js",
   "diy-crafts/app/home/home-controller.js",
   "diy-crafts/app/search/search-controller.js",
   "diy-crafts/app/search/search-service.js",
   "diy-crafts/app/common/app-controller.js",
   "diy-crafts/app/common/app-service.js",
   "diy-crafts/app/interceptors/http-api-url.js",
   "diy-crafts/app/common/app-route.js",
   "github:twbs/bootstrap@3.3.6/dist/js/bootstrap.js",
   "github:twbs/bootstrap@3.3.6.json",
   "npm:jquery@2.2.3/dist/jquery.js",
   "npm:jquery@2.2.3.json",
   "github:brandly/angular-youtube-embed@1.3.1/src/angular-youtube-embed.js",
   "github:brandly/angular-youtube-embed@1.3.1.json",
   "npm:angular-environment@1.0.3/dist/angular-environment.js",
   "npm:angular-environment@1.0.3.json",
   "github:angular-ui/angular-ui-router-bower@0.3.1/release/angular-ui-router.js",
   "github:angular-ui/angular-ui-router-bower@0.3.1.json",
   "npm:react@15.2.1/react.js",
   "npm:react@15.2.1.json",
   "github:jspm/nodelibs-process@0.2.0-alpha/process.js",
   "github:jspm/nodelibs-process@0.2.0-alpha.json",
   "npm:react@15.2.1/lib/React.js",
   "npm:react@15.2.1/lib/ReactElementValidator.js",
   "npm:fbjs@0.8.3/lib/warning.js",
   "npm:fbjs@0.8.3.json",
   "npm:fbjs@0.8.3/lib/emptyFunction.js",
   "npm:react@15.2.1/lib/getIteratorFn.js",
   "npm:react@15.2.1/lib/canDefineProperty.js",
   "npm:react@15.2.1/lib/checkReactTypeSpec.js",
   "npm:react@15.2.1/lib/ReactComponentTreeDevtool.js",
   "npm:fbjs@0.8.3/lib/invariant.js",
   "npm:react@15.2.1/lib/ReactCurrentOwner.js",
   "npm:react@15.2.1/lib/reactProdInvariant.js",
   "npm:react@15.2.1/lib/ReactPropTypeLocationNames.js",
   "npm:react@15.2.1/lib/ReactPropTypeLocations.js",
   "npm:fbjs@0.8.3/lib/keyMirror.js",
   "npm:react@15.2.1/lib/ReactElement.js",
   "npm:object-assign@4.1.0/index.js",
   "npm:object-assign@4.1.0.json",
   "npm:react@15.2.1/lib/onlyChild.js",
   "npm:react@15.2.1/lib/ReactVersion.js",
   "npm:react@15.2.1/lib/ReactPropTypes.js",
   "npm:react@15.2.1/lib/ReactDOMFactories.js",
   "npm:fbjs@0.8.3/lib/mapObject.js",
   "npm:react@15.2.1/lib/ReactClass.js",
   "npm:fbjs@0.8.3/lib/keyOf.js",
   "npm:fbjs@0.8.3/lib/emptyObject.js",
   "npm:react@15.2.1/lib/ReactNoopUpdateQueue.js",
   "npm:react@15.2.1/lib/ReactComponent.js",
   "npm:react@15.2.1/lib/ReactChildren.js",
   "npm:react@15.2.1/lib/traverseAllChildren.js",
   "npm:react@15.2.1/lib/KeyEscapeUtils.js",
   "npm:react@15.2.1/lib/PooledClass.js",
   "github:angular/bower-angular-animate@1.5.7/angular-animate.js",
   "github:angular/bower-angular-animate@1.5.7.json",
   "npm:core-js@2.4.1/index.js",
   "npm:core-js@2.4.1.json",
   "npm:core-js@2.4.1/modules/_core.js",
   "npm:core-js@2.4.1/modules/core.string.unescape-html.js",
   "npm:core-js@2.4.1/modules/_replacer.js",
   "npm:core-js@2.4.1/modules/_export.js",
   "npm:core-js@2.4.1/modules/_ctx.js",
   "npm:core-js@2.4.1/modules/_a-function.js",
   "npm:core-js@2.4.1/modules/_redefine.js",
   "npm:core-js@2.4.1/modules/_uid.js",
   "npm:core-js@2.4.1/modules/_has.js",
   "npm:core-js@2.4.1/modules/_hide.js",
   "npm:core-js@2.4.1/modules/_descriptors.js",
   "npm:core-js@2.4.1/modules/_fails.js",
   "npm:core-js@2.4.1/modules/_property-desc.js",
   "npm:core-js@2.4.1/modules/_object-dp.js",
   "npm:core-js@2.4.1/modules/_to-primitive.js",
   "npm:core-js@2.4.1/modules/_is-object.js",
   "npm:core-js@2.4.1/modules/_ie8-dom-define.js",
   "npm:core-js@2.4.1/modules/_dom-create.js",
   "npm:core-js@2.4.1/modules/_global.js",
   "npm:core-js@2.4.1/modules/_an-object.js",
   "npm:core-js@2.4.1/modules/core.string.escape-html.js",
   "npm:core-js@2.4.1/modules/core.regexp.escape.js",
   "npm:core-js@2.4.1/modules/core.number.iterator.js",
   "npm:core-js@2.4.1/modules/_iter-define.js",
   "npm:core-js@2.4.1/modules/_wks.js",
   "npm:core-js@2.4.1/modules/_shared.js",
   "npm:core-js@2.4.1/modules/_object-gpo.js",
   "npm:core-js@2.4.1/modules/_shared-key.js",
   "npm:core-js@2.4.1/modules/_to-object.js",
   "npm:core-js@2.4.1/modules/_defined.js",
   "npm:core-js@2.4.1/modules/_set-to-string-tag.js",
   "npm:core-js@2.4.1/modules/_iter-create.js",
   "npm:core-js@2.4.1/modules/_object-create.js",
   "npm:core-js@2.4.1/modules/_html.js",
   "npm:core-js@2.4.1/modules/_enum-bug-keys.js",
   "npm:core-js@2.4.1/modules/_object-dps.js",
   "npm:core-js@2.4.1/modules/_object-keys.js",
   "npm:core-js@2.4.1/modules/_object-keys-internal.js",
   "npm:core-js@2.4.1/modules/_array-includes.js",
   "npm:core-js@2.4.1/modules/_to-index.js",
   "npm:core-js@2.4.1/modules/_to-integer.js",
   "npm:core-js@2.4.1/modules/_to-length.js",
   "npm:core-js@2.4.1/modules/_to-iobject.js",
   "npm:core-js@2.4.1/modules/_iobject.js",
   "npm:core-js@2.4.1/modules/_cof.js",
   "npm:core-js@2.4.1/modules/_iterators.js",
   "npm:core-js@2.4.1/modules/_library.js",
   "npm:core-js@2.4.1/modules/core.object.make.js",
   "npm:core-js@2.4.1/modules/_object-define.js",
   "npm:core-js@2.4.1/modules/_own-keys.js",
   "npm:core-js@2.4.1/modules/_object-gops.js",
   "npm:core-js@2.4.1/modules/_object-gopn.js",
   "npm:core-js@2.4.1/modules/_object-gopd.js",
   "npm:core-js@2.4.1/modules/_object-pie.js",
   "npm:core-js@2.4.1/modules/core.object.define.js",
   "npm:core-js@2.4.1/modules/core.object.classof.js",
   "npm:core-js@2.4.1/modules/_classof.js",
   "npm:core-js@2.4.1/modules/core.object.is-object.js",
   "npm:core-js@2.4.1/modules/core.function.part.js",
   "npm:core-js@2.4.1/modules/_partial.js",
   "npm:core-js@2.4.1/modules/_invoke.js",
   "npm:core-js@2.4.1/modules/_path.js",
   "npm:core-js@2.4.1/modules/core.delay.js",
   "npm:core-js@2.4.1/modules/core.is-iterable.js",
   "npm:core-js@2.4.1/modules/core.get-iterator.js",
   "npm:core-js@2.4.1/modules/core.get-iterator-method.js",
   "npm:core-js@2.4.1/modules/core.dict.js",
   "npm:core-js@2.4.1/modules/_iter-step.js",
   "npm:core-js@2.4.1/modules/_for-of.js",
   "npm:core-js@2.4.1/modules/_is-array-iter.js",
   "npm:core-js@2.4.1/modules/_iter-call.js",
   "npm:core-js@2.4.1/modules/_keyof.js",
   "npm:core-js@2.4.1/modules/_object-assign.js",
   "npm:core-js@2.4.1/shim.js",
   "npm:core-js@2.4.1/modules/web.dom.iterable.js",
   "npm:core-js@2.4.1/modules/es6.array.iterator.js",
   "npm:core-js@2.4.1/modules/_add-to-unscopables.js",
   "npm:core-js@2.4.1/modules/web.immediate.js",
   "npm:core-js@2.4.1/modules/_task.js",
   "npm:core-js@2.4.1/modules/web.timers.js",
   "npm:core-js@2.4.1/modules/es7.observable.js",
   "npm:core-js@2.4.1/modules/_set-species.js",
   "npm:core-js@2.4.1/modules/_redefine-all.js",
   "npm:core-js@2.4.1/modules/_an-instance.js",
   "npm:core-js@2.4.1/modules/_microtask.js",
   "npm:core-js@2.4.1/modules/es7.asap.js",
   "npm:core-js@2.4.1/modules/es7.reflect.metadata.js",
   "npm:core-js@2.4.1/modules/_metadata.js",
   "npm:core-js@2.4.1/modules/es6.weak-map.js",
   "npm:core-js@2.4.1/modules/_collection.js",
   "npm:core-js@2.4.1/modules/_inherit-if-required.js",
   "npm:core-js@2.4.1/modules/_set-proto.js",
   "npm:core-js@2.4.1/modules/_iter-detect.js",
   "npm:core-js@2.4.1/modules/_meta.js",
   "npm:core-js@2.4.1/modules/_collection-weak.js",
   "npm:core-js@2.4.1/modules/_array-methods.js",
   "npm:core-js@2.4.1/modules/_array-species-create.js",
   "npm:core-js@2.4.1/modules/_array-species-constructor.js",
   "npm:core-js@2.4.1/modules/_is-array.js",
   "npm:core-js@2.4.1/modules/es6.map.js",
   "npm:core-js@2.4.1/modules/_collection-strong.js",
   "npm:core-js@2.4.1/modules/es7.reflect.has-own-metadata.js",
   "npm:core-js@2.4.1/modules/es7.reflect.has-metadata.js",
   "npm:core-js@2.4.1/modules/es7.reflect.get-own-metadata-keys.js",
   "npm:core-js@2.4.1/modules/es7.reflect.get-own-metadata.js",
   "npm:core-js@2.4.1/modules/es7.reflect.get-metadata-keys.js",
   "npm:core-js@2.4.1/modules/_array-from-iterable.js",
   "npm:core-js@2.4.1/modules/es6.set.js",
   "npm:core-js@2.4.1/modules/es7.reflect.get-metadata.js",
   "npm:core-js@2.4.1/modules/es7.reflect.delete-metadata.js",
   "npm:core-js@2.4.1/modules/es7.reflect.define-metadata.js",
   "npm:core-js@2.4.1/modules/es7.math.umulh.js",
   "npm:core-js@2.4.1/modules/es7.math.imulh.js",
   "npm:core-js@2.4.1/modules/es7.math.isubh.js",
   "npm:core-js@2.4.1/modules/es7.math.iaddh.js",
   "npm:core-js@2.4.1/modules/es7.error.is-error.js",
   "npm:core-js@2.4.1/modules/es7.system.global.js",
   "npm:core-js@2.4.1/modules/es7.set.to-json.js",
   "npm:core-js@2.4.1/modules/_collection-to-json.js",
   "npm:core-js@2.4.1/modules/es7.map.to-json.js",
   "npm:core-js@2.4.1/modules/es7.object.lookup-setter.js",
   "npm:core-js@2.4.1/modules/_object-forced-pam.js",
   "npm:core-js@2.4.1/modules/es7.object.lookup-getter.js",
   "npm:core-js@2.4.1/modules/es7.object.define-setter.js",
   "npm:core-js@2.4.1/modules/es7.object.define-getter.js",
   "npm:core-js@2.4.1/modules/es7.object.entries.js",
   "npm:core-js@2.4.1/modules/_object-to-array.js",
   "npm:core-js@2.4.1/modules/es7.object.values.js",
   "npm:core-js@2.4.1/modules/es7.object.get-own-property-descriptors.js",
   "npm:core-js@2.4.1/modules/_create-property.js",
   "npm:core-js@2.4.1/modules/es7.symbol.observable.js",
   "npm:core-js@2.4.1/modules/_wks-define.js",
   "npm:core-js@2.4.1/modules/_wks-ext.js",
   "npm:core-js@2.4.1/modules/es7.symbol.async-iterator.js",
   "npm:core-js@2.4.1/modules/es7.string.match-all.js",
   "npm:core-js@2.4.1/modules/_flags.js",
   "npm:core-js@2.4.1/modules/_is-regexp.js",
   "npm:core-js@2.4.1/modules/es7.string.trim-right.js",
   "npm:core-js@2.4.1/modules/_string-trim.js",
   "npm:core-js@2.4.1/modules/_string-ws.js",
   "npm:core-js@2.4.1/modules/es7.string.trim-left.js",
   "npm:core-js@2.4.1/modules/es7.string.pad-end.js",
   "npm:core-js@2.4.1/modules/_string-pad.js",
   "npm:core-js@2.4.1/modules/_string-repeat.js",
   "npm:core-js@2.4.1/modules/es7.string.pad-start.js",
   "npm:core-js@2.4.1/modules/es7.string.at.js",
   "npm:core-js@2.4.1/modules/_string-at.js",
   "npm:core-js@2.4.1/modules/es7.array.includes.js",
   "npm:core-js@2.4.1/modules/es6.reflect.set-prototype-of.js",
   "npm:core-js@2.4.1/modules/es6.reflect.set.js",
   "npm:core-js@2.4.1/modules/es6.reflect.prevent-extensions.js",
   "npm:core-js@2.4.1/modules/es6.reflect.own-keys.js",
   "npm:core-js@2.4.1/modules/es6.reflect.is-extensible.js",
   "npm:core-js@2.4.1/modules/es6.reflect.has.js",
   "npm:core-js@2.4.1/modules/es6.reflect.get-prototype-of.js",
   "npm:core-js@2.4.1/modules/es6.reflect.get-own-property-descriptor.js",
   "npm:core-js@2.4.1/modules/es6.reflect.get.js",
   "npm:core-js@2.4.1/modules/es6.reflect.enumerate.js",
   "npm:core-js@2.4.1/modules/es6.reflect.delete-property.js",
   "npm:core-js@2.4.1/modules/es6.reflect.define-property.js",
   "npm:core-js@2.4.1/modules/es6.reflect.construct.js",
   "npm:core-js@2.4.1/modules/_bind.js",
   "npm:core-js@2.4.1/modules/es6.reflect.apply.js",
   "npm:core-js@2.4.1/modules/es6.typed.float64-array.js",
   "npm:core-js@2.4.1/modules/_typed-array.js",
   "npm:core-js@2.4.1/modules/_array-copy-within.js",
   "npm:core-js@2.4.1/modules/_array-fill.js",
   "npm:core-js@2.4.1/modules/_species-constructor.js",
   "npm:core-js@2.4.1/modules/_same-value.js",
   "npm:core-js@2.4.1/modules/_typed-buffer.js",
   "npm:core-js@2.4.1/modules/_typed.js",
   "npm:core-js@2.4.1/modules/es6.typed.float32-array.js",
   "npm:core-js@2.4.1/modules/es6.typed.uint32-array.js",
   "npm:core-js@2.4.1/modules/es6.typed.int32-array.js",
   "npm:core-js@2.4.1/modules/es6.typed.uint16-array.js",
   "npm:core-js@2.4.1/modules/es6.typed.int16-array.js",
   "npm:core-js@2.4.1/modules/es6.typed.uint8-clamped-array.js",
   "npm:core-js@2.4.1/modules/es6.typed.uint8-array.js",
   "npm:core-js@2.4.1/modules/es6.typed.int8-array.js",
   "npm:core-js@2.4.1/modules/es6.typed.data-view.js",
   "npm:core-js@2.4.1/modules/es6.typed.array-buffer.js",
   "npm:core-js@2.4.1/modules/es6.weak-set.js",
   "npm:core-js@2.4.1/modules/es6.promise.js",
   "npm:core-js@2.4.1/modules/es6.regexp.split.js",
   "npm:core-js@2.4.1/modules/_fix-re-wks.js",
   "npm:core-js@2.4.1/modules/es6.regexp.search.js",
   "npm:core-js@2.4.1/modules/es6.regexp.replace.js",
   "npm:core-js@2.4.1/modules/es6.regexp.match.js",
   "npm:core-js@2.4.1/modules/es6.regexp.flags.js",
   "npm:core-js@2.4.1/modules/es6.regexp.to-string.js",
   "npm:core-js@2.4.1/modules/es6.regexp.constructor.js",
   "npm:core-js@2.4.1/modules/es6.array.species.js",
   "npm:core-js@2.4.1/modules/es6.array.find-index.js",
   "npm:core-js@2.4.1/modules/es6.array.find.js",
   "npm:core-js@2.4.1/modules/es6.array.fill.js",
   "npm:core-js@2.4.1/modules/es6.array.copy-within.js",
   "npm:core-js@2.4.1/modules/es6.array.last-index-of.js",
   "npm:core-js@2.4.1/modules/_strict-method.js",
   "npm:core-js@2.4.1/modules/es6.array.index-of.js",
   "npm:core-js@2.4.1/modules/es6.array.reduce-right.js",
   "npm:core-js@2.4.1/modules/_array-reduce.js",
   "npm:core-js@2.4.1/modules/es6.array.reduce.js",
   "npm:core-js@2.4.1/modules/es6.array.every.js",
   "npm:core-js@2.4.1/modules/es6.array.some.js",
   "npm:core-js@2.4.1/modules/es6.array.filter.js",
   "npm:core-js@2.4.1/modules/es6.array.map.js",
   "npm:core-js@2.4.1/modules/es6.array.for-each.js",
   "npm:core-js@2.4.1/modules/es6.array.sort.js",
   "npm:core-js@2.4.1/modules/es6.array.slice.js",
   "npm:core-js@2.4.1/modules/es6.array.join.js",
   "npm:core-js@2.4.1/modules/es6.array.of.js",
   "npm:core-js@2.4.1/modules/es6.array.from.js",
   "npm:core-js@2.4.1/modules/es6.array.is-array.js",
   "npm:core-js@2.4.1/modules/es6.date.to-primitive.js",
   "npm:core-js@2.4.1/modules/_date-to-primitive.js",
   "npm:core-js@2.4.1/modules/es6.date.to-string.js",
   "npm:core-js@2.4.1/modules/es6.date.to-iso-string.js",
   "npm:core-js@2.4.1/modules/es6.date.to-json.js",
   "npm:core-js@2.4.1/modules/es6.date.now.js",
   "npm:core-js@2.4.1/modules/es6.string.sup.js",
   "npm:core-js@2.4.1/modules/_string-html.js",
   "npm:core-js@2.4.1/modules/es6.string.sub.js",
   "npm:core-js@2.4.1/modules/es6.string.strike.js",
   "npm:core-js@2.4.1/modules/es6.string.small.js",
   "npm:core-js@2.4.1/modules/es6.string.link.js",
   "npm:core-js@2.4.1/modules/es6.string.italics.js",
   "npm:core-js@2.4.1/modules/es6.string.fontsize.js",
   "npm:core-js@2.4.1/modules/es6.string.fontcolor.js",
   "npm:core-js@2.4.1/modules/es6.string.fixed.js",
   "npm:core-js@2.4.1/modules/es6.string.bold.js",
   "npm:core-js@2.4.1/modules/es6.string.blink.js",
   "npm:core-js@2.4.1/modules/es6.string.big.js",
   "npm:core-js@2.4.1/modules/es6.string.anchor.js",
   "npm:core-js@2.4.1/modules/es6.string.starts-with.js",
   "npm:core-js@2.4.1/modules/_fails-is-regexp.js",
   "npm:core-js@2.4.1/modules/_string-context.js",
   "npm:core-js@2.4.1/modules/es6.string.repeat.js",
   "npm:core-js@2.4.1/modules/es6.string.includes.js",
   "npm:core-js@2.4.1/modules/es6.string.ends-with.js",
   "npm:core-js@2.4.1/modules/es6.string.code-point-at.js",
   "npm:core-js@2.4.1/modules/es6.string.iterator.js",
   "npm:core-js@2.4.1/modules/es6.string.trim.js",
   "npm:core-js@2.4.1/modules/es6.string.raw.js",
   "npm:core-js@2.4.1/modules/es6.string.from-code-point.js",
   "npm:core-js@2.4.1/modules/es6.math.trunc.js",
   "npm:core-js@2.4.1/modules/es6.math.tanh.js",
   "npm:core-js@2.4.1/modules/_math-expm1.js",
   "npm:core-js@2.4.1/modules/es6.math.sinh.js",
   "npm:core-js@2.4.1/modules/es6.math.sign.js",
   "npm:core-js@2.4.1/modules/_math-sign.js",
   "npm:core-js@2.4.1/modules/es6.math.log2.js",
   "npm:core-js@2.4.1/modules/es6.math.log1p.js",
   "npm:core-js@2.4.1/modules/_math-log1p.js",
   "npm:core-js@2.4.1/modules/es6.math.log10.js",
   "npm:core-js@2.4.1/modules/es6.math.imul.js",
   "npm:core-js@2.4.1/modules/es6.math.hypot.js",
   "npm:core-js@2.4.1/modules/es6.math.fround.js",
   "npm:core-js@2.4.1/modules/es6.math.expm1.js",
   "npm:core-js@2.4.1/modules/es6.math.cosh.js",
   "npm:core-js@2.4.1/modules/es6.math.clz32.js",
   "npm:core-js@2.4.1/modules/es6.math.cbrt.js",
   "npm:core-js@2.4.1/modules/es6.math.atanh.js",
   "npm:core-js@2.4.1/modules/es6.math.asinh.js",
   "npm:core-js@2.4.1/modules/es6.math.acosh.js",
   "npm:core-js@2.4.1/modules/es6.number.parse-int.js",
   "npm:core-js@2.4.1/modules/_parse-int.js",
   "npm:core-js@2.4.1/modules/es6.number.parse-float.js",
   "npm:core-js@2.4.1/modules/_parse-float.js",
   "npm:core-js@2.4.1/modules/es6.number.min-safe-integer.js",
   "npm:core-js@2.4.1/modules/es6.number.max-safe-integer.js",
   "npm:core-js@2.4.1/modules/es6.number.is-safe-integer.js",
   "npm:core-js@2.4.1/modules/_is-integer.js",
   "npm:core-js@2.4.1/modules/es6.number.is-nan.js",
   "npm:core-js@2.4.1/modules/es6.number.is-integer.js",
   "npm:core-js@2.4.1/modules/es6.number.is-finite.js",
   "npm:core-js@2.4.1/modules/es6.number.epsilon.js",
   "npm:core-js@2.4.1/modules/es6.number.to-precision.js",
   "npm:core-js@2.4.1/modules/_a-number-value.js",
   "npm:core-js@2.4.1/modules/es6.number.to-fixed.js",
   "npm:core-js@2.4.1/modules/es6.number.constructor.js",
   "npm:core-js@2.4.1/modules/es6.parse-float.js",
   "npm:core-js@2.4.1/modules/es6.parse-int.js",
   "npm:core-js@2.4.1/modules/es6.function.has-instance.js",
   "npm:core-js@2.4.1/modules/es6.function.name.js",
   "npm:core-js@2.4.1/modules/es6.function.bind.js",
   "npm:core-js@2.4.1/modules/es6.object.to-string.js",
   "npm:core-js@2.4.1/modules/es6.object.set-prototype-of.js",
   "npm:core-js@2.4.1/modules/es6.object.is.js",
   "npm:core-js@2.4.1/modules/es6.object.assign.js",
   "npm:core-js@2.4.1/modules/es6.object.is-extensible.js",
   "npm:core-js@2.4.1/modules/_object-sap.js",
   "npm:core-js@2.4.1/modules/es6.object.is-sealed.js",
   "npm:core-js@2.4.1/modules/es6.object.is-frozen.js",
   "npm:core-js@2.4.1/modules/es6.object.prevent-extensions.js",
   "npm:core-js@2.4.1/modules/es6.object.seal.js",
   "npm:core-js@2.4.1/modules/es6.object.freeze.js",
   "npm:core-js@2.4.1/modules/es6.object.get-own-property-names.js",
   "npm:core-js@2.4.1/modules/_object-gopn-ext.js",
   "npm:core-js@2.4.1/modules/es6.object.keys.js",
   "npm:core-js@2.4.1/modules/es6.object.get-prototype-of.js",
   "npm:core-js@2.4.1/modules/es6.object.get-own-property-descriptor.js",
   "npm:core-js@2.4.1/modules/es6.object.define-properties.js",
   "npm:core-js@2.4.1/modules/es6.object.define-property.js",
   "npm:core-js@2.4.1/modules/es6.object.create.js",
   "npm:core-js@2.4.1/modules/es6.symbol.js",
   "npm:core-js@2.4.1/modules/_enum-keys.js"
  ]
 }
});
