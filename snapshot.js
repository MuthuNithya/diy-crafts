var htmlSnapshots = require('html-snapshots');
var result = htmlSnapshots.run({
    input: "array",
    source: ["http://localhost:8000"],
    outputDir: "./snapshots",
    outputDirClean: true,
    selector: "#dynamic-content"
}, function(err, snapshotsCompleted) {
    var fs = require('fs');
    fs.rename('/snapshots/#!', '/snapshots/views', function(err) {
        if ( err ) console.log('ERROR: ' + err);
    });
});