/**
 * Created by nithyarad on 7/31/16.
 */

class DetailsController {
    /*@ngInject*/
    constructor($scope,$state,$stateParams,DiyCraftsService){
        this.$scope = $scope;
        this.$state = $state;
        this.$stateParam = $stateParams;
        this.projectDetails = {};
        this.videoID = '';
        this.DiyService = DiyCraftsService;
        angular.element('body').removeClass('projectPage homePage ContactPage detailsPage').addClass('detailsPage');
        this.init();
    }
    init(){
        if(this.$stateParam && this.$stateParam.id){
            this.DiyService.getProjectDetails(this, this.$stateParam.id);
        }
    }
    shareProject(shareOpt){
        let facebookLinkBasic = 'http://www.facebook.com/sharer.php?s=100',
            pinterestLinkBasic = 'http://pinterest.com/pin/create/button/?url=',//jshint ignore:line
            googlePlusLinkBasic = 'https://plus.google.com/share?';//jshint ignore:line

        let projectName = this.projectDetails.name.trim(),compiledFullShareText = '';
        switch (shareOpt) {
            case 'facebook':
                compiledFullShareText = facebookLinkBasic + '&p[title]=' + encodeURIComponent(projectName) + '&p[summary]=' + encodeURIComponent(this.projectDetails.description) + '&p[video]=' + encodeURIComponent(this.projectDetails.videoUrl);
                break;
            /*case 'pinterest':
             messageToPost = messageToPost + ' ' + storeName + '. #' + hashTag;
             compiledFullShareText += pinterestLinkBasic + encodeURIComponent(shortUrl) + '&media=' + encodeURIComponent(imgUrl) + '&description=' + encodeURIComponent(messageToPost);
             break;
             case 'googlePlus':
             if ('VideoArticle' == pageType) {
             compiledFullShareText += googlePlusLinkBasic + 'url=' + encodeURIComponent(bitlyUrl);
             } else {
             compiledFullShareText += googlePlusLinkBasic + 'url=' + encodeURIComponent(shortUrl);
             }
             break;*/
        }
        if ('email' === shareOpt) {
            var emailSubject = 'I found something for you';
            var itemUrl = bitlyUrl ? bitlyUrl : shortUrl;//jshint ignore:line
            messageToPost = messageToPost.replace(/'/g, '') + ' #' + hashTag + ': ' + itemUrl; //jshint ignore:line
            popEmailShareModal(encodeURIComponent(emailSubject), messageToPost);//jshint ignore:line
        } else if ('pinterest' === shareOpt ) { //using pin it plugin type except PDP,Qv and bundle page
            var e = document.createElement('script');
            e.setAttribute('type', 'text/javascript');
            e.setAttribute('charset', 'UTF-8');
            e.setAttribute('src', 'http://assets.pinterest.com/js/pinmarklet.js?r=' + Math.random() * 99999999);
            document.body.appendChild(e);
        } else {
            FB.init({//jshint ignore:line
                appId      : '1768197653435213',
                status     : true,
                xfbml      : true,
                version    : 'v2.7'
            });//jshint ignore:line
            FB.ui({//jshint ignore:line
                method: 'share',
                display: 'popup',
                href: this.projectDetails.videoUrl
            }, function(response){});//jshint ignore:line
        }
    }
}
export default DetailsController;