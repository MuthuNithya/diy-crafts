/**
 * Created by nithyarad on 7/18/16.
 */
import * as constants from 'src/app/common/consts.js';

class ProjectController{
    /*@ngInject*/
    constructor($scope, $rootScope, ProjectService){
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.projects = [];
        this.projectSrv = ProjectService;
        this.showNoResult = false;
        this.thumbnailBasePath = constants.thumbnailBasePath;
        if(angular.element('.searchHeaderContainer').length>0){
            angular.element('.searchHeaderContainer').removeClass('hide');
        }
        angular.element('body').removeClass('projectPage homePage ContactPage detailsPage').addClass('projectPage');
        this.init();
        this.$scope.$on('search-result', function(event, args) {
            event.currentScope.projectCtrl.projects = args.projects;
            if(angular.equals({},args.projects)){
                event.currentScope.projectCtrl.showNoResult = true;
            } else{
                event.currentScope.projectCtrl.showNoResult = false;
            }
        });
    }
    init(){
        this.projectSrv.getProjectsList(this);
    }
}
export default ProjectController;