class ProjectService{
    /*@ngInject*/
    constructor(DiyCraftsService){
        this.DiyCraftsServ = DiyCraftsService;
    }
    getProjectsList(model){
        this.DiyCraftsServ.getProjectsList(model);
    }
}

export default ProjectService;
