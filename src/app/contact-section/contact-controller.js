/**
 * Created by nithyarad on 7/18/16.
 */
'use strict';
//import * as constants from 'src/app/common/consts.js';
class ContactController{
    /*@ngInject*/
    constructor($scope,contactService,$q){
        this.$scope = $scope;
        this.contactServ = contactService;
        this.$serviceError = false;
        this.$contactSuccess = false;
        this.$q = $q;
        if(angular.element('.searchHeaderContainer').length>0){
            angular.element('.searchHeaderContainer').addClass('hide');
        }
        angular.element('body').removeClass('projectPage homePage contactPage detailsPage').addClass('contactPage');
        this.init();
    }
    init(){

    }
    recommentUs() {
        this.$scope.contactCtrl.recomment.$submitted = true;
        if (this.$scope.contactCtrl.recomment.$valid) {
            angular.element('body').addClass('loading');
            let data = '{"name":"' + this.$scope.contactCtrl.recommentData.name + '","email":"' + this.$scope.contactCtrl.recommentData.email + '","subject":"' + this.$scope.contactCtrl.recommentData.subject + '","comment":"' + this.$scope.contactCtrl.recommentData.recomment + '"}';
            let contactData = this.contactServ.contactus(data, 'recomment');
            let all = this.$q.all([contactData]), self = this;
            all.then(function (data) {
                if (data[0] && data[0].status) {
                    if (data[0].status === 'success') {
                        self.$serviceError = false;
                        self.$contactSuccess = true;
                    } else {
                        self.$serviceError = true;
                        self.$contactSuccess = false;
                    }
                }
                angular.element('body').removeClass('loading');
            }, function () {
                self.$serviceError = true;
                self.$contactSuccess = false;
                angular.element('body').removeClass('loading');
            });
        }
    }
    contactUs() {
        this.$scope.contactCtrl.contact.$submitted = true;
        if (this.$scope.contactCtrl.contact.$valid) {
            angular.element('body').addClass('loading');
            let data = '{"name":"'+this.$scope.contactCtrl.contactData.name+'","email":"'+this.$scope.contactCtrl.contactData.email+'","subject":"'+this.$scope.contactCtrl.contactData.subject+'","comment":"'+this.$scope.contactCtrl.contactData.comment+'"}';
            let contactData = this.contactServ.contactus(data,'contact');
            let all = this.$q.all([contactData]), self = this;
            all.then(function (data) {
                if (data[0] && data[0].status) {
                    if (data[0].status === 'success') {
                        self.$serviceError = false;
                        self.$contactSuccess = true;
                    } else {
                        self.$serviceError = true;
                        self.$contactSuccess = false;
                    }
                }
                angular.element('body').removeClass('loading');
            }, function () {
                self.$serviceError = true;
                self.$contactSuccess = false;
                angular.element('body').removeClass('loading');
            });
        }
    }
}

export default ContactController;
