/**
 * Created by nithyarad on 7/30/16.
 */
import * as constants from 'src/app/common/consts.js';
class contactService{
  /*@ngInject*/
    constructor($http, $q, $state, $rootScope,DiyCraftsService){
        this.$http = $http;
        this.$q = $q;
        this.$state = $state;
        this.$rootScope = $rootScope;
        this.DiyService = DiyCraftsService;
    }

    contactus(inputdata,option){
        let api,deferred = this.$q.defer(),target;
        if(option === 'contact'){
            api = 'api/v1/users/contact';
            target='DIYCRAFTS_CONTACTUS';
        } else{
            api = 'api/v1/users/suggest';
            target='DIYCRAFTS_SUGGEST';
        }

        this.DiyService.call(constants.serverEndPoint+api,inputdata, {'x-diy-key': true,'X-DIYCRAFTS-TARGET':target}, 'POST')
            .then(response =>{
                if(response && response.data){
                    deferred.resolve(response.data);
                }else{
                    deferred.reject(response.data);
                }
            }, ()=>{
                deferred.reject();
            });
        return deferred.promise;
    }
}
export default contactService;
