/**
 * Created by nithyarad on 7/29/16.
 */
'use strict';

let self;

class HttpInterceptor{
    /*@ngInject*/
    constructor($q, envService, $injector, $rootScope){
        self = this;
        self.apiUrl = envService.read('apiUrl');
        self.envService = envService;
        self.$injector = $injector;
        self.$q = $q;
        self.$rootScope = $rootScope;
    }

    /**
     * Intercepts the http request and performs config changes
     * @param config
     * @returns {*}
     */
    request(config){
        if(self.$rootScope){
            self.numberOfHttpRequests += 1;
            self.$rootScope.waitingForHttp = true;
        }

        /*if(config.url){
            config.headers.Accept = 'application/json';
            config.headers['Content-Type'] = 'application/json';

            if(self.envService && self.envService.get() === 'local'){
                config.method = 'GET';
            }else {
                if(config.headers){
                    config.url = self.apiUrl;
                    if(self.sharedService.tokenEncrypted){
                        config.headers.tokenEncrypted = self.sharedService.tokenEncrypted;
                    }
                    if(self.sharedService.tokenTimeStamp){
                        config.headers.tokenTimeStamp = self.sharedService.tokenTimeStamp;
                    }
                }
            }

            if(config.method === 'GET' && !config.data){
                config.data = '';
            }
        }*/

        return config;
    }

    /**
     * Intercepts the response and explodes the data
     * @param resp
     * @returns {*}
     */

    response(resp){
        /*if(self.$rootScope){
            self.numberOfHttpRequests -=1;
            self.$rootScope.waitingForHttp = (self.numberOfHttpRequests !== 0);
        }
        if(resp && resp.data && resp.data){
            const responseHeaders = resp.headers();*/
            /*if(!self.sharedService){
                self.sharedService = self.$injector.get('sharedService');
            }
            if(responseHeaders['accept-language']){
                let bLocale = responseHeaders['accept-language'];

                let comma = responseHeaders['accept-language'].indexOf(',');
                if(comma > 0){
                    bLocale = bLocale.subString(0, comma);
                }

                self.sharedService.shared.browserLanguage = bLocale;
            }*/
            /*if(responseHeaders.tokenEncrypted){
                self.sharedService.tokenEncrypted = responseHeaders.tokenEncrypted;
            }
            if(responseHeaders.tokenTimeStamp){
                self.sharedService.tokenTimeStamp = responseHeaders.tokenTimeStamp;
            }*/
            //if(resp.config.headers['Content-Type'] && resp.config.headers['Content-Type'].includes(HEADER_API_CONTENT_TYPE) X-DIYCRAFTS-PROJECT){
            if(resp.config.headers['X-DIYCRAFTS-PROJECT']){
                return resp.data;
            }
        //}

        /*if(resp && resp.data && resp.data.faults){
            return self.$q.reject(resp);
        }*/
        return resp;
    }

    responseError(response){
        if(self.$rootscope){
            self.numberOfHttpRequests -= 1;
            self.$rootscope.waitingForHttp = (self.numberOfHttpRequests !== 0);
        }
        if(response && response.config && response.config.headers['X-RDV-TARGET']){
            if(response.data && response.data.faults){
                return self.$q.reject(response);
            }
        }
        return self.$q.reject(response);
    }

    requestError(){
        if(self.$rootscope){
            self.numberOfHttpRequests -= 1;
            self.$rootscope.waitingForHttp = (self.numberOfHttpRequests !== 0);
        }
    }
}

export default HttpInterceptor;
