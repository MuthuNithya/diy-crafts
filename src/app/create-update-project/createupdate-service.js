class createupdateService {
    /*@ngInject*/
    constructor($http, $q) {
        this.$http = $http;
        this.$q = $q;
    }
    errorCallback(response){
        this.reject(response);
    }

    successCallback(response){
        this.resolve(response);
    }
    projectsList() {
            let deferred = this.$q.defer();
            this.$http({
                method: 'GET',
                //url:'https://heroku-node-server.herokuapp.com/api/v1/worksheets/create',
                //url: 'http://192.168.1.68:5000/api/v1/projects/projectsList',
                url:'http://localhost:5000/api/v1/projects/projectsList',
                'Content-Type': 'application/json',
                headers: {
                    'x-diy-key': true,
                    'X-DIYCRAFTS-TARGET': 'GET_PROJECTS'
                }
            }).then(this.successCallback.bind(deferred), this.errorCallback.bind(deferred));
            return deferred.promise;
        }

        projectAddition(data, target) {
            let deferred = this.$q.defer();
            this.$http({
                method: 'POST',
                //url:'https://heroku-node-server.herokuapp.com/api/v1/worksheets/create',
                //url: 'http://192.168.1.68:5000/api/v1/projects/create',
                url:'http://localhost:5000/api/v1/projects/create',
                'Content-Type': 'application/json',
                data: data,
                headers: {
                    'x-diy-key': true,
                    'X-DIYCRAFTS-TARGET': target
                },
                config: {
                    'timeout': 1000
                }
            }).then(this.successCallback.bind(deferred), this.errorCallback.bind(deferred));
            return deferred.promise;
        }
}

export default createupdateService;
