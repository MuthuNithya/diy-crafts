'use strict';
import * as constants from '../common/consts';
class createupdateController {
    /*@ngInject*/
    constructor($scope, createupdateService, $q, $modal, $cookies, $state, $anchorScroll, $location, FileUploader) {
        this.showprojectLoading = true;
        this.procedureInput = [1];
        this.showLoading = false;
        this.showSuccess = false;
        this.showFailure = false;
        this.successMsg = constants.projectMessages.createSuccess;
        this.failureMsg = constants.projectMessages.createFailure;
        this.pendingChanges = false;
        this.buttonText = 'Save';
        this.currentState = 'create';
        this.fileName = '';
        this.$state = $state;
        this.createUpdateSrv = createupdateService;
        this.$q = $q;
        this.$index = '';
        //var id = $state.params.sid;

        this.uploader = new FileUploader({
            headers: {
                'x-diy-key': true,
                'X-DIYCRAFTS-TARGET': 'DIYCRAFTS_CREATE'
            },
            url: 'http://localhost:5000/api/v1/projects/create'
            //url: 'http://192.168.1.68:5000/api/v1/projects/create'

        });

        this.imageUploader = new FileUploader({
            headers: {
                'x-diy-key': true,
                'X-DIYCRAFTS-MODE': 'MEMBER',
                'X-DIYCRAFTS-TARGET': 'PROJECT_IMAGE'
            },
            url: 'http://localhost:5000/api/v1/projects/upload/image',
            //url: 'http://192.168.1.68:5000/api/v1/projects/upload/image',
            autoUpload: true,
            //removeAfterUpload:true
        });

        this.init(this);
    }
    init(that){
        that.imageUploader.filters.push({
            name: 'imageFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {//jshint ignore:line
                var size = item.size;
                if (size <= 1000000) {
                    return true;
                } else {
                    that.imageUploader.cancelItem(item);
                    return false;
                }
            }
        });

        that.uploader.onAfterAddingFile = function (fileItem) {
            that.fileName = fileItem.file.name;
        };
        that.uploader.onAfterAddingAll = function (addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
            that.fileName = addedFileItems[0].file.name;
        };
        that.uploader.onBeforeUploadItem = function (item) {
            console.info('onBeforeUploadItem', item);
        };

        that.imageUploader.onSuccessItem = function (item, response, status, headers,options) {//jshint ignore:line
            console.info('item = ', item);
            console.info('response = ', response);
            console.log('index= ', that.imageUploader.getIndexOfItem(item));
            console.info('status = ', status);
            that.createData.procedure[that.$index].photo.push(response.uploadPath);
        };

        that.imageUploader.onError = function (item, response, status, headers) {//jshint ignore:line
            console.info('response = ', response);
            console.info('status = ', status);
            console.log('index= ', that.imageUploader.getIndexOfItem(item));
        };

        that.imageUploader.onCancelItem = function (item, response, status, headers) {//jshint ignore:line
            that.imageUploader.removeFromQueue(item);
        };

        $('[data-toggle="tooltip"]').tooltip();

        if (that.$state.current.name === 'create') {
            $("[name='photoGuideAvailable']").bootstrapSwitch();//jshint ignore:line
            $("[name='videoGuideAvailable']").bootstrapSwitch();//jshint ignore:line
            $("[name='isImageVertical']").bootstrapSwitch();//jshint ignore:line
        }

        $(document).on('click','input[type="file"]',function () {
            that.$index = $(this).attr('data-index');
        });
        that.createData = {
            "name": "",//jshint ignore:line
            "description": "",//jshint ignore:line
            "materialsRequired": [],//jshint ignore:line
            "procedure": [{"operation":"","photo":[]}],//jshint ignore:line
            "procedureImage": [""],//jshint ignore:line
            "photoGuideAvailable": false,//jshint ignore:line
            "videoGuideAvailable": true,//jshint ignore:line
            "videoUrl": "",//jshint ignore:line
            "videoId": "",//jshint ignore:line
            "thumbnailName": "",//jshint ignore:line
            "isImageVertical": false,//jshint ignore:line
            "className": "",//jshint ignore:line
            "tags": [],//jshint ignore:line
            "urlName": "",//jshint ignore:line
            "projectId": "",//jshint ignore:line
            "publishDate": ""//jshint ignore:line
        };
        that.createStringByArray = function (array) {
            var output = '';
            angular.forEach(array, function (value, key) {//jshint ignore:line
                output += value + ',';
            });
            return output;
        };
        if (that.$state.$current.name === 'update') {
            let $pid = that.$state.params.pid,
                projectList = angular.fromJson(sessionStorage.projectList), self = that;

            angular.forEach(projectList, function (project, index) {//jshint ignore:line
                if (project.projectId === $pid) {
                    self.createData = project;
                    self.createData.materialsRequired = self.createStringByArray(project.materialsRequired);
                    if (project.photoGuideAvailable) {
                        $("[name='photoGuideAvailable']").bootstrapSwitch('state', true);//jshint ignore:line
                    } else {
                        $("[name='photoGuideAvailable']").bootstrapSwitch();//jshint ignore:line
                    }
                    if (project.videoGuideAvailable) {
                        $("[name='videoGuideAvailable']").bootstrapSwitch('state', true);//jshint ignore:line
                    } else {
                        $("[name='videoGuideAvailable']").bootstrapSwitch();//jshint ignore:line
                    }
                    if (project.isImageVertical) {
                        $("[name='isImageVertical']").bootstrapSwitch('state', true);//jshint ignore:line
                    } else {
                        $("[name='isImageVertical']").bootstrapSwitch();//jshint ignore:line
                    }
                }
            });
            that.buttonText = 'Update';
            that.currentState = 'update';
            that.pendingChanges = true;
        }

        $('input[name="photoGuideAvailable"]').on('switchChange.bootstrapSwitch', function (event, state) {
            that.createData.photoGuideAvailable = state;
        });
        $('input[name="videoGuideAvailable"]').on('switchChange.bootstrapSwitch', function (event, state) {
            that.createData.videoGuideAvailable = state;
        });
        $('input[name="isImageVertical"]').on('switchChange.bootstrapSwitch', function (event, state) {
            that.createData.isImageVertical = state;
        });
        $(document).on('keypress', function () {
            that.pendingChanges = true;
        });

        that.addNewChoice = function () {
            var newItemNo = that.createData.procedure.length + 1;//jshint ignore:line
            that.createData.procedure.push({'operation':'','photo':[]});
        };

        that.removeChoice = function (index) {
            if (index !== 0) {
                that.createData.procedure.splice(index, 1);
            }
        };

        that.cancelProject = function () {
            if (that.pendingChanges) {
                $('#pendingChangesModal').modal({
                    keyboard: false
                });
            } else {
                that.$state.go('home');
            }
        };

        that.createProject = function () {
            that.formCreate.$submitted = true;
            var target;
            if (that.pendingChanges) {
                if (that.formCreate.$valid) {
                    if (typeof that.createData.materialsRequired === 'string') {
                        that.createData.materialsRequired = that.createData.materialsRequired.split(',');
                    }
                    if (typeof that.createData.tags === 'string') {
                        that.createData.tags = that.createData.tags.split(',');
                    }
                    that.createData.urlName = that.createData.name.replace(/[\s]/g, '-');
                    if (that.createData.isImageVertical) {
                        that.createData.className = 'vertical-image';
                    }
                    if (that.currentState === 'update') {
                        target = 'DIYCRAFTS_UPDATE';
                    } else {
                        target = 'DIYCRAFTS_CREATE';
                    }
                    console.log(that.createData);
                    $('#loadingModal').modal({
                        keyboard: false
                    });
                    let self = that;
                    var fetchedData = that.createUpdateSrv.projectAddition(that.createData, target);
                    var all = that.$q.all([fetchedData]);
                    all.then(function (data) {
                        if (data[0] && data[0].projectId) {
                            $('#loadingModal').modal('hide');
                            self.showSuccess = true;
                            self.showFailure = false;
                            self.successMsg = 'Project created successfully.';
                            self.pendingChanges = false;
                            $location.hash('successMsgSection');//jshint ignore:line
                            $anchorScroll();//jshint ignore:line
                        } else {
                            //$scope.errorMsg = data[0].err_msg || data[0].message;
                            $('#loadingModal').modal('hide');
                            self.showSuccess = false;
                            self.showFailure = true;
                            self.failureMsg = 'Something went wrong. Please try again later.';
                            $location.hash('errorMsgSection');//jshint ignore:line
                            $anchorScroll();//jshint ignore:line
                        }
                    }, function (reject) {//jshint ignore:line
                        $('#loadingModal').modal('hide');
                        self.showSuccess = false;
                        self.showFailure = true;
                        self.failureMsg = 'Something went wrong. Please try again later.';
                        $location.hash('errorMsgSection');//jshint ignore:line
                        $anchorScroll();//jshint ignore:line
                    });
                }
            }
        };
        let self = that;
        var fetchedData = that.createUpdateSrv.projectsList();
        var all = that.$q.all([fetchedData]);
        all.then(function (data) {
            if (data[0]) {
                self.showprojectLoading = false;
                self.projectList = data[0];
                sessionStorage.projectList = angular.toJson(data[0]);
            } else {
                //$scope.errorMsg = data[0].err_msg || data[0].message;
                self.showprojectLoading = false;
                self.projectList = data;
            }
        }, function (reject) {//jshint ignore:line
            self.showprojectLoading = false;
        });
    }
}

export default createupdateController;