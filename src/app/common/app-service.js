/**
 * Created by nithyarad on 7/30/16.
 */
import * as constants from 'src/app/common/consts.js';
class DiyCraftsService{
  /*@ngInject*/
    constructor($http, $q, $injector, $rootScope,$location,$anchorScroll){
        this.$http = $http;
        this.$q = $q;
        this.$state = $injector.get('$state');
        this.$rootScope = $rootScope;
        this.$location = $location;
        this.$anchorScroll = $anchorScroll;
    }
    call(url, data, header, method, params){
        let deferred = this.$q.defer();
        if(!method){
            method = 'POST';
        }
        this.$http({
            method: method,
            url: url,
            data: data,
            headers: header,
            params: params
        }).then(this.successCallback.bind(deferred), this.errorCallback.bind(deferred));

        return deferred.promise;
    }

    errorCallback(response){
        this.reject(response);
    }

    successCallback(response){
        this.resolve(response);
    }

    getProjectsList(model){
        let that = this;
        this.call('/data/projects.json', {}, {'X-DIYCRAFTS-PROJECT':'DIY-PROJECTS'}, 'GET')
            .then(response =>{
                if(response && response.data && response.data.length > 0){
                    model.projects = response.data;
                    if(that.$state && that.$state.name !== 'home'){
                        that.$rootScope.enableSearch = true;
                    }
                }else{
                    model.projects = [];
                }
            }, ()=>{
                model.projects = [];
            });
    }

    subscribeUser(name,emailId){
        let deferred = this.$q.defer();

        this.call(constants.serverEndPoint+'api/v1/users/subscribe',{'email':emailId,'name':name}, {'x-diy-key': true,'X-DIYCRAFTS-TARGET':'DIYCRAFTS_SUBSCRIBE'}, 'POST')
            .then(response =>{
                if(response && response.data){
                    deferred.resolve(response.data);
                }else{
                    deferred.reject(response.data);
                }
            }, ()=>{
                deferred.reject();
            });
        return deferred.promise;
    }

    gotoElement(id) {
        // set the location.hash to the id of
        // the element you wish to scroll to.
        this.$location.hash(id);

        // call $anchorScroll()
        this.$anchorScroll();
    }
    getProjectDetails(model, projectId){
        if(projectId){
            this.call(constants.detailsBasePath+projectId+'.json', {}, {'X-DIYCRAFTS-PROJECT':'LATEST-ADDITIONS'}, 'GET')
                .then(response=>{
                    if(response && response && response.id === projectId){
                        model.projectDetails = response;
                        model.videoId = response.videoId;
                    } else {
                        model.projectDetails = '';
                        model.videoId = '';
                    }
                }, ()=>{
                    model.projectDetails = '';
                    model.videoId = '';
                });
        }
    }
}
export default DiyCraftsService;
