'use strict';

export function closeModal(){
  return {
    restrict: 'A',
    link: function(scope, elem){
      angular.element(elem).on('click', function(){
        angular.element('body').removeClass('modal-open');
      });
    }
  };
}
