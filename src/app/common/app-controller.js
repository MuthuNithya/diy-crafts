'use strict';

class DiyCraftsController{
    /*@ngInject*/
    constructor($scope, DiyCraftsService,$q,$modal,$cookies){
        this.$scope = $scope;
        this.diySrv = DiyCraftsService;
        this.$q=$q;
        this.$modal=$modal;
        this.$serviceError = false;
        this.$subscribeSuccess = false;
        this.$cookies = $cookies;
        if(angular.element('.searchHeaderContainer').length>0){
            angular.element('.searchHeaderContainer').addClass('hide');
        }
        let curStep = $cookies.get('myTour');
        this.currentStep=0;
        if(typeof curStep === 'string') {
            curStep = parseInt(curStep);
        }

        this.currentStep =  curStep || 0;
    }
    postTourCallback() {
        angular.element('html').removeClass('tour-guide-on');
        this.currentStep=-1;
        if(this.$cookies.get('diyCraftsSubscribeShown') === undefined){
            this.showSubscribeModal();
            this.$cookies.put('diyCraftsSubscribeShown',true);
        }
    }

    tourCompleteCallback() {
        angular.element('html').removeClass('tour-guide-on');
        if(this.$cookies.get('diyCraftsSubscribeShown') === undefined){
            this.showSubscribeModal();
            this.$cookies.put('diyCraftsSubscribeShown',true);
        }
    }

    postStepCallback() {
        this.$cookies.put('myTour', this.currentStep||0);
        if(this.currentStep === 6){
            angular.element('html').addClass('tour-guide-on');
        }
    }
    gotoElement(id){
        this.diySrv.gotoElement(id);
    }
    showSubscribeModal() {
        this.$scope.myModal = this.$modal({scope:this.$scope,templateUrl: 'src/app/layout/subscribe-modal.html', show: false});
        this.$scope.myModal.$promise.then(this.$scope.myModal.show);
    }
    hideSubscribeModal() {
        this.$scope.myModal.$promise.then(this.$scope.myModal.hide);
    }
    subscribeuser(){
        this.$scope.DiyCtrl.subscribe.$submitted=true;
        if(this.$scope.DiyCtrl.subscribe.$valid){
            angular.element('body').addClass('loading');
            let subscribeData = this.diySrv.subscribeUser(this.$scope.DiyCtrl.subscribeName,this.$scope.DiyCtrl.subscribeEmail);
            let all = this.$q.all([subscribeData]),self=this;
            all.then(function (data) {
                if (data[0] && data[0].status) {
                    if (data[0].status === 'success') {
                        self.$serviceError = false;
                        self.$subscribeSuccess = true;
                    } else {
                        self.$serviceError = true;
                        self.$subscribeSuccess = false;
                    }
                }
                angular.element('body').removeClass('loading');
            }, function () {
                self.$serviceError = true;
                self.$subscribeSuccess = false;
                angular.element('body').removeClass('loading');
            });
        }
    }
}

export default DiyCraftsController;
