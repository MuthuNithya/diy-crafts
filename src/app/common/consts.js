/**
 * Created by nithyarad on 7/28/16.
 */

export const thumbnailBasePath = '/images/project-images/';
export const DEV_API_URL = '';
export const API_URL = '';
export const detailsBasePath = '/data/project-details/';
export const detailsImagePath = '/images/project-details/';
export const projectMessages = {
    createSuccess: 'Project created successfully.', 
    createFailure: 'Something went wrong. Please try again later.'
};
export const serverEndPoint = 'https://diycrafts-services.herokuapp.com/';
//export const serverEndPoint = 'http://localhost:5000/'; //jshint ignore:line
