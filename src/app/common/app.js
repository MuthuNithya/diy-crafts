'use strict';

import 'core-js';
import 'jquery';
import 'angular';
import 'angular-animate';
import 'react';
import 'angular-ui-router/release/angular-ui-router';
import 'angular-environment/dist/angular-environment';
import 'angular-youtube-embed';
import 'bootstrap';
import 'angular-strap';
import 'angular-sanitize';
import 'font-awesome/css/font-awesome.min.css!';
import 'angular-cookies';
import 'angular-tour';
import 'jspm_packages/bower/re-tree@0.0.2/re-tree.min.js';
import 'ng-device-detector';
import 'angular-file-upload';
import 'bootstrap-switch';
import diyCrafts from './app.config.js';

class AppBoostrap{
    constructor(){
        angular.element(document).ready(AppBoostrap.bootstrap);
    }

    static bootstrap(){
        angular.bootstrap(document, [diyCrafts.name], {
            strictDi: false
        });
    }
}

new AppBoostrap();

export default AppBoostrap;


