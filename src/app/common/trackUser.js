var realOpen = window.XMLHttpRequest.prototype.open,
    realSend = window.XMLHttpRequest.prototype.send,
    onReadyStateChange,
    $data = {
    url: '',
    method: '',
    status: '',
    header: '',
    request: '',
    response: ''
};
function openReplacement(method, url, async, user, password) {
    var syncMode = async !== false ? 'async' : 'sync';
    console.warn('Preparing ' + syncMode + ' HTTP request : ' + method + ' ' + url);
    $data = {url: '', method: '', status: '', header: '', request: '', response: ''};
    $data.url = url;
    $data.method = method;
    return realOpen.apply(this, arguments);
}
function sendReplacement(data) {
    console.warn('Sending HTTP request data : ', data);
    $data.request = data;
    if (this.onreadystatechange) {
        this._onreadystatechange = this.onreadystatechange;
    }
    this.onreadystatechange = onReadyStateChangeReplacement;
    console.log('$data1: ' + $data.request);
    return realSend.apply(this, arguments);
}
function onReadyStateChangeReplacement() {
    console.warn('HTTP request ready state changed : ' + this.readyState);
    $data.status = this.status;
    if (this.responseType === '' || this.responseType === 'text') {
        $data.response = this.responseText;
    }
    if (this._onreadystatechange) {
        console.log('$data2: ' + $data.response);
        return this._onreadystatechange.apply(this, arguments);
    }
}
window.XMLHttpRequest.prototype.open = openReplacement;
window.XMLHttpRequest.prototype.send = sendReplacement;

function createSnapshot() {
    html2canvas(document.body, {
        onrendered: function(canvas) {
            document.body.appendChild(canvas);
        }
    });
}

window.addEventListener("load", createSnapshot);