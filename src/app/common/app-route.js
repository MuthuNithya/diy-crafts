'use strict';

/*@ngInject*/
function appConfig($stateProvider,$urlRouterProvider,$locationProvider){
    //$locationProvider.hashPrefix('!');
  /*@ngInject*/
    $urlRouterProvider.otherwise(function ($injector) {
        var _$state = $injector.get('$state');
      _$state.go('home');
    });
    $urlRouterProvider.when('/home','home');
    $urlRouterProvider.when('/projects','projects');
    $locationProvider.html5Mode({enabled:true,requireBase:false}).hashPrefix('!');
    $stateProvider
        .state('home',{
            url:'/home',
            templateUrl:'src/app/home/home.html',
            controller:'homeController',
            controllerAs:'homeCtrl'
        })
        .state('projects',{
            url:'/projects',
            templateUrl:'src/app/projects/project.html',
            controller:'ProjectController',
            controllerAs:'projectCtrl'
        })
        .state('details',{
            url:'/details/:name/:id',
            templateUrl:'src/app/projects/project-details/project-details.html',
            controller:'DetailsController',
            controllerAs:'detailsCtrl'
        })
        .state('contactus',{
            url:'/contactus',
            templateUrl:'src/app/contact-section/contact-us.html',
            controller:'ContactController',
            controllerAs:'contactCtrl'
        })
        .state('suggest',{
            url:'/suggest',
            templateUrl:'src/app/contact-section/recommend.html',
            controller:'ContactController',
            controllerAs:'contactCtrl'
        })
        .state('dashboard',{
            url:'/dashboard',
            templateUrl:'src/app/create-update-project/dashboard.html',
            controller:'createupdateController',
            controllerAs:'createupdateCtrl'
        })
        .state('create',{
            url:'/create',
            templateUrl:'src/app/create-update-project/create-update.html',
            controller:'createupdateController',
            controllerAs:'createupdateCtrl'
        })
        .state('update',{
            url:'/update:pid',
            templateUrl:'src/app/create-update-project/create-update.html',
            controller:'createupdateController',
            controllerAs:'createupdateCtrl'
        });
}

export default appConfig;
