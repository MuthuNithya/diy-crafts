/**
 * Created by nithyarad on 7/17/16.
 */
'use strict';
import * as constants from './consts.js';
import appConfig from './app-route.js';
import httpInterceptor from '../interceptors/http-api-url.js';
import DiyCraftsService from './app-service.js';
import DiyCraftsController from './app-controller.js';
import SearchService from '../search/search-service.js';
import SearchController from 'src/app/search/search-controller.js';
import HomeController from 'src/app/home/home-controller.js';
import HomeService from '../home/home-service.js';
import ContactController from '../contact-section/contact-controller.js';
import contactService from '../contact-section/contact-service.js';
import ProjectService from '../projects/project-service.js';
import ProjectController from '../projects/project-controller.js';
import DetailsController from '../projects/project-details/details-controller.js';
import createupdateController from '../create-update-project/createupdate-controller.js';
import createupdateService from '../create-update-project/createupdate-service.js';
import {closeModal} from './app-directives.js';
//import {scrollElement} from 'src/app/home/home-directive.js';
import 'src/app/layout/header.html!ng-template';
import 'src/app/layout/mobile-aside-nav.html!ng-template';
import 'src/app/layout/subscribe-modal.html!ng-template';
import 'src/app/home/home.html!ng-template';
import 'src/app/home/home-projects.html!ng-template';
import 'src/app/search/search.html!ng-template';
import 'src/app/projects/project-details/project-details.html!ng-template';
import 'src/app/contact-section/contact-us.html!ng-template';
import 'src/app/contact-section/recommend.html!ng-template';
import 'src/app/projects/project.html!ng-template';
import 'src/app/layout/footer.html!ng-template';

let diyCrafts = angular.module('diy-crafts', ['ui.router','angularFileUpload','angular-tour','reTree','ng.deviceDetector','ngCookies', 'ngSanitize','environment','youtube-embed', 'mgcrea.ngStrap.aside','mgcrea.ngStrap.modal']);
diyCrafts.service('httpInterceptor', httpInterceptor);
diyCrafts.service('DiyCraftsService', DiyCraftsService);
diyCrafts.service('HomeService', HomeService);
diyCrafts.service('SearchService',SearchService);
diyCrafts.service('ProjectService', ProjectService);
diyCrafts.service('contactService',contactService);
diyCrafts.service('createupdateService',createupdateService);
diyCrafts.controller('SearchController',SearchController);
diyCrafts.controller('DiyCraftsController',DiyCraftsController);
diyCrafts.controller('homeController', HomeController);
diyCrafts.controller('ProjectController', ProjectController);
diyCrafts.controller('DetailsController',DetailsController);
diyCrafts.controller('ContactController',ContactController);
diyCrafts.controller('createupdateController',createupdateController);
diyCrafts.directive('closeModal', closeModal);
/*diyCrafts.directive('scrollElement',function($window) {
    return{
        restrict: 'A',
        link: function(scope) {
            console.log('test send');
            angular.element($window).bind("scroll", function() {
                if (this.pageYOffset >= 300) {
                    if(angular.element('.homePage .diy-header').length>0)
                        angular.element('.homePage .diy-header').css('background','#c41d4f !important');
                }
                scope.$apply();
            });
        }
    };
});*/
diyCrafts.config(appConfig);
diyCrafts.config(($httpProvider, envServiceProvider)=>{
    $httpProvider.interceptors.push('httpInterceptor');
    envServiceProvider.config({
        domains:{
            local:['localhost'],
            dev:['dev.corp.apple.com'],
            development: ['diycraftsnme.com'],
            production: ['diycraftsnme.com']
        },
        vars:{
            local:{
                apiUrl: ''
            },
            dev:{
                apiUrl: constants.DEV_API_URL
            },
            qa:{
                apiUrl: constants.API_URL
            },
            development: {apiUrl:'diycraftsnme.com'},
            production: {apiUrl:'diycraftsnme.com'}
        }
    });
    envServiceProvider.check();

});
export default diyCrafts;
