/*@ngInject*/
'use strict';
export function scrollElement($window) {
    console.log('test ');
    return{
        restrict: 'A',
        link: function(scope) {
            console.log('test send');
            angular.element($window).bind('scroll', function() {
                if (this.pageYOffset >= 300) {
                    if(angular.element('.homePage .diy-header').length>0) {
                        angular.element('.homePage .diy-header').css('background', '#c41d4f !important');
                    }
                }
                scope.$apply();
            });
        }
    };
}