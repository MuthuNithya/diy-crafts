/**
 * Created by nithyarad on 7/28/16.
 */

class HomeService{
    /*@ngInject*/
    constructor($http, $q, DiyCraftsService){
        this.$http = $http;
        this.$q = $q;
        this.DiyService = DiyCraftsService;
    }
    getLatestAdditions(model){
        this.DiyService.call('/data/latest-additions.json',{}, {'X-DIYCRAFTS-PROJECT':'LATEST-ADDITIONS'}, 'GET')
        .then(response =>{
            if(response && response.data && response.data.length > 0){
                model.latestAdditions = response.data;
            }else{
                model.latestAdditions = [];
            }
        }, ()=>{
            model.latestAdditions = [];
        });
    }

    getHomeProjectsList(model){
        this.DiyService.call('/data/projects.json', {}, {'X-DIYCRAFTS-PROJECT':'HOME-PROJECTS'}, 'GET')
        .then(response =>{
            if(response && response.data && response.data.length > 0){
                model.homeProjects = response.data;
            }else{
                model.homeProjects = [];
            }
        }, ()=>{
            model.homeProjects = [];
        });
    }
}

export default HomeService;
