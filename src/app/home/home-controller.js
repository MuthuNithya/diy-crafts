/**
 * Created by nithyarad on 7/18/16.
 */
'use strict';
import * as constants from 'src/app/common/consts.js';
class HomeController{
    /*@ngInject*/
    constructor($scope, HomeService){
        this.$scope = $scope;
        this.homeSrv = HomeService;
        this.thumbnailBasePath = constants.thumbnailBasePath;
        this.latestAdditions = [];
        this.homeProjects = [];
        if(angular.element('.searchHeaderContainer').length>0){
            angular.element('.searchHeaderContainer').addClass('hide');
        }
        angular.element('body').removeClass('projectPage homePage contactPage detailsPage').addClass('homePage');

        /*if($cookies.get('diyCraftsSubscribeShown') == undefined){
            angular.element('#subscribeLink').click();
            $cookies.put('diyCraftsSubscribeShown',true);
        }*/

        this.init();
    }
    init(){
        this.homeSrv.getLatestAdditions(this);
        this.homeSrv.getHomeProjectsList(this);

    }
}

export default HomeController;
