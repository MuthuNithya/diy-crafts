/**
 * Created by nithyarad on 7/30/16.
 */
//import * as constants from 'src/app/common/consts.js';
class SearchService{
    /*@ngInject*/
    constructor(DiyCraftsService){
        this.DiyCraftsService = DiyCraftsService;
    }
    getProjectsList(model){
        this.DiyCraftsService.getProjectsList(model);
    }
}

export default SearchService;