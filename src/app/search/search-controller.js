/**
 * Created by nithyarad on 7/18/16.
 */
import * as constants from 'src/app/common/consts.js';

class SearchController{
    /*@ngInject*/
    constructor($scope, $rootScope, SearchService){
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.projects = [];
        this.SearchSrv = SearchService;
        this.thumbnailBasePath = constants.thumbnailBasePath;
        this.showSearchResults = false;
        this.noresultsfound = false;
        this.SearchResults = {};
        this.init();
    }
    init(){
        this.SearchSrv.getProjectsList(this);
    }
    Search(){
        let SearchTerms = this.searchTerms;
        //let Projects = this.projects;
            let output;
        let options =  {
            keys: ['name', 'description','tags'],
            shouldSort: true,
            tokenize: true,
            matchAllTokens: true,
            threshold: 0.2
        };
        let fuse = new Fuse(this.projects, options),$searchHeaderContainer = angular.element('.searchHeaderContainer'); //jshint ignore:line
        if(SearchTerms  && SearchTerms!==''){
            output = fuse.search(SearchTerms);
            if(output.length>0){
                if(!angular.element('body').hasClass('homePage')){
                    this.$rootScope.$broadcast('search-result', {projects:output});
                } else{
                    this.SearchResults = output;
                    this.showSearchResults = true;
                    this.noresultsfound = false;
                }
            } else{
                if(!angular.element('body').hasClass('homePage')){
                    this.$rootScope.$broadcast('search-result', {projects:{}});
                } else{
                    this.showSearchResults = true;
                    this.noresultsfound = true;
                    this.SearchResults = {};
                }
            }
        } else{
            if(!$searchHeaderContainer.hasClass('hide')) {
                this.SearchSrv.getProjectsList(this);
                this.$rootScope.$broadcast('search-result', {projects:this.projects});
            } else{
                this.showSearchResults = false;
                this.noresultsfound = false;
                this.SearchResults = {};
            }
        }

    }
}
export default SearchController;
