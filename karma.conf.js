/**
 * Created by nithyarad on 7/19/16.
 */
'use strict';

var nocoverage = false, coveralls = false, pattern;
process.argv.forEach(function(val){
   if(val.indexOf('--nocoverage') !== -1 || val.indexOf('--no-coverage') !== -1){
       nocoverage = true;
   }
    if(val.indexOf('--coveralls') !== -1){
        coveralls = true;
    }
    if(val.indexOf('--pattern') !== -1){
        pattern = val.substring(val.indexOf('=')+1, val.length);
    }
});

module.exports = function(config){
  if(!pattern){
      pattern = 'src/app/**/*.spec.js';
  }
    var globArray = require('glob-array');
    var loadFiles = globArray.sync(['src/app/common/app.js', pattern]);
    var serveFiles = globArray.sync(['src/app/**/*.+(js|html|css|json)']);
    config.set({
        frameworks: ['jspm', 'jasmine'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        singleRun: false,
        files:[
            'jspm_packages/bower/karma-read-json@1.1.0/karma-read-json.js',
            'node_modules/babel-polyfill/browser.js',
            'node_modules/jasmine-async-sugar/jasmine-async-sugar.js'
        ],
        jspm:{
            useBundles: false,
            cachePackages: true,
            stripExtenstion: false,
            config: 'jspm.config.js',
            browser: 'jspm.browser.js',
            loadFiles: loadFiles,
            serveFiles: serveFiles
        },
        proxies:{
            '/test/': '/base/test/',
            '/data': '/basa/src/data/',
            '/src/app/': '/base/src/app/',
            'jspm_packages':'/base/jspm_packages/'
        },
        reporters:['dots', 'progress', 'junit', 'coverage', 'coveralls', 'spec'],
        junitReporter:{
            outputDir: 'test-reports/unit-test-report',
            suite: 'unit'
        },
        preprocessors: {
            'src/**/!(*.spec|*.mock|*.-mock|*.e2e|*.po|*.test).js': ['babel', 'coverage']
        },
        babelPreprocessor:{
            options:{
                presets: ['es2015'],
                sourcMap: 'inline'
            }
        },
        coverageReporter:{
            dir: 'test-reports/coverage/',
            subdir: normalizationBrowserName,
            reporters:[
                {type: 'html'},//will generate html report
                {type: 'json'},//will generate json report file and this report is loaded to make sure failed coverage cause gulp to exit non-zero
                {type: 'lcov'}, //will generate Icov report file and this report is published to coveralls
                {type: 'text-summary'}, // it does not generate any file but it will print coverage to console
                {type: 'json-summary', file:'coverage.json'} // it does not generate any file but it will print coverage to console

            ]
        },
        broswers: ['PhantomJS'],
        borwserNoActivityTimeout: 120000

    });
    function normalizationBrowserName(browser){
        return browser.toLowerCase().split(/[/-]/)[0];
    }
};